public class App {
    public static void main(String[] args) throws Exception {
        Account account1 = new Account("abc", "Nam");
        Account account2 = new Account("def", "Hoa", 1000);
        System.out.println(account1);
        System.out.println(account2);
        System.out.println("");
        System.out.println("Tang tai khoan 1 them 2000, luc nay co: " + account1.credit(2000));
        System.out.println("Tai khoan 1 bay gio la:");
        System.out.println(account1);
        System.out.println("Tang tai khoan 2 them 3000, luc nay co: " + account2.credit(3000));
        System.out.println("Tai khoan 2 bay gio la:");
        System.out.println(account2);
        System.out.println("");
        System.out.println("Giam tai khoan 1 di 1000, luc nay co: " + account1.debit(1000));
        System.out.println("Tai khoan 1 bay gio la:");
        System.out.println(account1);
        System.out.println("Giam tai khoan 2 di 5000");
        account2.debit(5000);
        System.out.println("Tai khoan 2 bay gio la:");
        System.out.println(account2);
        System.out.println("");
        System.out.println("Chuyen 2000 tu tai khoan 1 sang tai khoan 2");
        account1.transferTo(account2, 2000);
        System.out.println("Tai khoan 1 bay gio la:");
        System.out.println(account1);
        System.out.println("Tai khoan 2 bay gio la:");
        System.out.println(account2);
        System.out.println("");
        System.out.println("Chuyen 2000 tu tai khoan 2 sang tai khoan 1");
        account2.transferTo(account1, 2000);
        System.out.println("Tai khoan 1 bay gio la:");
        System.out.println(account1);
        System.out.println("Tai khoan 2 bay gio la:");
        System.out.println(account2);
    }
}
