public class Account {
    String id;
    String name;
    int balance = 0;
    // khởi tạo có 2 tham số id và name
    public Account(String id, String name) {
        this.id = id;
        this.name = name;
    }
    // khởi tạo có đủ các tham số
    public Account(String id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }
    // getter
    public String getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public int getBalance() {
        return balance;
    }
    // method thêm tiền vào tài khoản
    public int credit(int amount) {
        balance += amount;
        return balance;
    }
    // method trừ tiền trong tài khoản
    public int debit(int amount) {
        if(amount <= balance) {
            balance =  balance - amount;
        }
        else {
            System.out.println("Amount exceeded balance");
        }
        return balance;
    }
    // method chuyển tiền sang tài khoản khác
    public int transferTo(Account another, int amount) {
        if(amount <= balance) {
            this.debit(amount);
            another.credit(amount);
        }
        else {
            System.out.println("Amount exceeded balance");
        }
        return balance;
    }
    @Override
    public String toString() {
        return "Account [id=" + id
        + ", name=" + name
        + ", balance=" + balance + "]";
    }
}
